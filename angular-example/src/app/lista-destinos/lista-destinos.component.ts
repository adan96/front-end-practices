import { Component, OnInit } from '@angular/core';
import { DestinoViaje } from '../models/DestinoViaje.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {

  destinos: DestinoViaje[];//Crea un vector de la clase DestinoViaje

  constructor() {
    this.destinos = [];
   }

  ngOnInit(): void {
  }

  guardar(nombre: string, url: string): boolean{
    this.destinos.push(new DestinoViaje(nombre, url));//Agrega lo que recibe del html al vector que está en el constructor
    return false;
  }

}